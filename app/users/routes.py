from werkzeug.security import generate_password_hash
from app.users import bp  # Assuming you have a users blueprint defined in app/users/__init__.py
from app.extensions import db
from app.models.user import User, UserIn, UserOut
 
####
## view functions
####    
@bp.get('/<int:user_id>')
@bp.output(UserOut)
def get_user(user_id):
    return db.get_or_404(User, user_id)
 
@bp.get('/')
@bp.output(UserOut(many=True))
def get_users():
    return User.query.all()
 
@bp.post('/')
@bp.input(UserIn, location='json')
@bp.output(UserOut, status_code=201)
def create_user(json_data):
    # Hash the password
    hashed_password = generate_password_hash(json_data['password'])
 
    # Replace the plaintext password with the hashed password
    json_data['password'] = hashed_password
 
    # Create user object
    user = User(**json_data)
 
    # Add user to the database
    db.session.add(user)
    db.session.commit()
 
    return user
 
@bp.patch('/<int:user_id>')
@bp.input(UserIn(partial=True), location='json')
@bp.output(UserOut)
def update_user(user_id, json_data):
    user = db.get_or_404(User, user_id)
    for attr, value in json_data.items():
        setattr(user, attr, value)
    db.session.commit()
    return user
 
@bp.delete('/<int:user_id>')
@bp.output({}, status_code=204)
def delete_user(user_id):
    user = db.get_or_404(User, user_id)
    db.session.delete(user)
    db.session.commit()
    return ''