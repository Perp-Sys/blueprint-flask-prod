from apiflask import Schema
from apiflask.fields import Integer, String
from apiflask.validators import Length, OneOf

from app.extensions import db
from app.models.registration import Registration

class UserIn(Schema):
    name = String(required=True, validate=Length(128))
    email = String(required=True, unique=True, validate=Length(128))
    password = String(required=True, validate=Length(256))

class UserOut(Schema):
    id = Integer()
    name = String()
    email = String()
    password = String()

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    email = db.Column(db.String(128))
    password = db.Column(db.String(256))